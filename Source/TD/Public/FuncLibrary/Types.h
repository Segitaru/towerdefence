// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"


UENUM(BlueprintType)
enum class ELineSpawner : uint8
{
	Left UMETA(DisplayName = "Left"),
	Middle UMETA(DisplayName = "Middle"),
	Right UMETA(DisplayName = "Right"),
};

UENUM(BlueprintType)
enum class ETypeBuild : uint8
{
	TranquilityTower UMETA(DisplayName = "Tranquility Tower"),
	BarracksOfConfidence UMETA(DisplayName = "Barracks Of Confidence"),
	TowerOfUnderstanding UMETA(DisplayName = "Tower Of Understanding"),
};

UENUM(BlueprintType)
enum class EAbilityType : uint8
{
	Attack UMETA(DisplayName = "Attack"),
	Defence UMETA(DisplayName = "Defence"),
	Economic UMETA(DisplayName = "Economic"),
	Other UMETA(DisplayName = "Other"),
};
UENUM(BlueprintType)
enum class EAbilityParametrType : uint8
{
	DecressCostTower UMETA(DisplayName = "DecressCostTower"),

	IncreaseDamageSpawnedUnit UMETA(DisplayName = "IncreaseDamageSpawnedUnit"),
	IncreaseSpeedAttackSpawnedUnit UMETA(DisplayName = "IncreaseSpeedAttackSpawnedUnit"),
	IncreaseHealthSpawnedUnit UMETA(DisplayName = "IncreaseHealthSpawnedUnit"),
	IncreaseSpeedMovedSpawnedUnit UMETA(DisplayName = "IncreaseSpeedMovedSpawnedUnit"),

	IncreaseSpeedAttackTower UMETA(DisplayName = "IncreaseSpeedAttackTower"),
	IncreaseRadiusTower UMETA(DisplayName = "IncreaseRadiusTower"),

	IncreaseProjectileDamage UMETA(DisplayName = "IncreaseProjectileDamage"),
	IncreaseProjectileSpeed UMETA(DisplayName = "IncreaseProjectileSpeed"),
	IncreaseProjectileRadius UMETA(DisplayName = "IncreaseProjectileRadius"),
	IncreaseProjectileSlowdownValue UMETA(DisplayName = "IncreaseProjectileSlowdownValue"),
	IncreaseProjectileSlowdownTime UMETA(DisplayName = "IncreaseProjectileSlowdownTime"),

	DecressEnergyValueToReadySkill UMETA(DisplayName = "DecressEnergyValueToReadySkill"),

	IncreaseScoreValue UMETA(DisplayName = "IncreaseScoreValue"),
};


UENUM(BlueprintType)
enum class EStageGame : uint8
{
	PreGame UMETA(DisplayName = "PreGame"),
	Game UMETA(DisplayName = "Game"),
	PostGame UMETA(DisplayName = "PostGame"),
};

UENUM(BlueprintType)
enum class EWaveGame : uint8
{
	Wave1 UMETA(DisplayName = "Wave1"),
	Wave2 UMETA(DisplayName = "Wave2"),
	Wave3 UMETA(DisplayName = "Wave3"),
	Wave4 UMETA(DisplayName = "Wave4"),
};

USTRUCT(BlueprintType)
struct FProjectileInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TSubclassOf<class ATDProjectileBase> Projectile = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		UParticleSystem* ProjectileHitFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		FTransform ProjectileHitTransform = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		USoundBase* ProjectileHitSound = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileDamage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileSpeed = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileLifeTime = 6.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		FVector ScaleProjectile = FVector(1);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float Projectile_AOE_RadiusDamage = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float Projectile_AOE_SlowdownValue = 0.95f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float Projectile_AOE_SlowdownTime = 1.f;
};

USTRUCT(BlueprintType)
struct FEnemyInfo : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		TSubclassOf<class ATDBaseCharacter_Enemy> EnemyClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		float CharacterHealth = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		float CostingEnemy = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		float DamageForBuilding = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		float DamageToActor = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		float BaseSpeed = 400.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		int LevelEnemy = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		UParticleSystem* OverlapEmmiter = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyInfo")
		USoundBase* OverlapSound = nullptr;



};

USTRUCT(BlueprintType)
struct FBuildInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		ETypeBuild TypeBuild;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		int32 LevelBuild=0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		TSubclassOf<class ATDBaseBuilding> BuildingClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float Damage = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float Cost = 500.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float SpeedAttack1 = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float SpeedAttack2 = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float SpeedAttack3= 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float RadiusAttack1 = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float RadiusAttack2 = 500.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float RadiusAttack3 = 700.f;



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float HealthDefender = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float SpeedAttackDefender = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		float SpeedDefaultMovement = 400.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		TSubclassOf<class ATDBaseCharacter> CharacterOnBuild = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		TSubclassOf<class AWeaponActor> ActorOnBuild = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		TSubclassOf<class ATDProjectileBase> PorjectileBuild = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		FText NameBuild;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		FText AdditionalInfoBuild;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		UTexture2D* IconBuild;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		FProjectileInfo ProjectileSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		int32 MaxValueDefender = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingInfo")
		TSubclassOf<class ATDBaseCharacter_Defender> DefenderPawn = nullptr;

};

USTRUCT(BlueprintType)
struct FLevelInfo : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		FName LevelNameToOpen;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		bool IsSuccess;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		FText LevelNameForUI;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		UTexture2D* IconLevel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		UTexture2D* LevelScreenHistory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		int32 FinalPhase = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		TArray<int32> ScoreToWinPhase = { 5,10,15 };
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		TSubclassOf<class ATDBaseCharacter> Boss;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		int32 AllStarsValue = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		int32 CurrentStarsValue = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		float AwardForStars = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		int32 ChapterLevel = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		int32 MinLevelEnemy = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		int32 MaxLevelEnemy = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelInfo")
		float StartedValueExp = 0;

};

UCLASS()
class TD_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
