// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile/TDProjectileBase_AOE.h"
#include "TDProjectileBase_AOE_Slowdown.generated.h"

/**
 * 
 */
UCLASS()
class TD_API ATDProjectileBase_AOE_Slowdown : public ATDProjectileBase_AOE
{
	GENERATED_BODY()
public:
		virtual void AdditionalLogicProjectile_AOE(const FHitResult& Hit) override;
};
