// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDProjectileBase.generated.h"

UCLASS()
class TD_API ATDProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDProjectileBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereCollision = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UParticleSystemComponent* ProjectileFX = nullptr;
	UPROPERTY(BlueprintReadOnly)
		FProjectileInfo ProjectileSetting;
	UPROPERTY()
		AActor* TargetToKill;
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	void ProjectileInit(AActor* TargetToKillRef, FProjectileInfo ProjectileSettingToInit);

	UFUNCTION()
	void ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	/*Function implementation logic interact projectile with enemy*/
	virtual void AdditionalLogicProjectile(const FHitResult& Hit);
	virtual bool CheckTagsActor(AActor* OtherActor);
	UFUNCTION()
		void DestroyProjectile(AActor* TargetOwner);

	UFUNCTION()
		void OnProjectileBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
