// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile/TDProjectileBase.h"
#include "TDProjectileBase_AOE.generated.h"

/**
 * 
 */
UCLASS()
class TD_API ATDProjectileBase_AOE : public ATDProjectileBase
{
	GENERATED_BODY()
	
public:
	virtual void AdditionalLogicProjectile(const FHitResult& Hit) override;
	virtual void AdditionalLogicProjectile_AOE(const FHitResult& Hit) {};
};
