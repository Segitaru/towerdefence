// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDBaseSpawner.generated.h"

UCLASS()
class TD_API ATDBaseSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDBaseSpawner();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USplineComponent* SplineComponent;

	FEnemyInfo EnemyInfoToSpawn;

	UPROPERTY()
		TSubclassOf<class ATDBaseCharacter_Enemy> EnemyClassToSpawn;
	UPROPERTY()
		class ATDGameModeBase* GameModeRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingSpawner")
		ELineSpawner LineSpawner = ELineSpawner::Left;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnEnemy(FEnemyInfo Info);
};
