// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameActor/TDBaseBuilding.h"
#include "TDBaseBuilding_SpawnerDefence.generated.h"

/**
 * 
 */
UCLASS()
class TD_API ATDBaseBuilding_SpawnerDefence : public ATDBaseBuilding
{
	GENERATED_BODY()
public:
	virtual void ImplemetationLogicTower() override;
	int32 SpawnedDefenderValue = 0;
	UFUNCTION()
		void DefenderDeath(AActor* TargetDefender);
	TArray<ATDBaseCharacter_Defender*> ArrayDefender;

	virtual void TowerLogicStop() override;
};
