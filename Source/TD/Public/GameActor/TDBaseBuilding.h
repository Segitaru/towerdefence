// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDBaseBuilding.generated.h"

UCLASS()
class TD_API ATDBaseBuilding : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDBaseBuilding();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereCollision = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MeshBuild = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* ArrowComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* SceneComponentSpawned = nullptr;
	class AActor* TargetToKill = nullptr;
	TArray<class AActor*> ArrayTarget;

	FTimerHandle TimerHandle_Shooting;
protected:
	int32 LevelBuild = 1;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	float CurrentRadiusAttack = 100.f;
	float SpeedAttackCurrent = 2.f;
	FProjectileInfo ProjectileSetting;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void InitBuild(FBuildInfo BuildingInfo);

	UPROPERTY(BlueprintReadOnly)
	FBuildInfo InfoBuilding;

	UFUNCTION()
		void OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnEnemyEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
		
		void TowerLogicStart();
	UFUNCTION()
		void TowerLogicTick();
		virtual void ImplemetationLogicTower();
	//UFUNCTION()
	//	void TargetKill(AActor* EnemyOwner);
	UFUNCTION()
		virtual void TowerLogicStop();
	UFUNCTION(BlueprintPure)
		float GetRadiusAttack() { return CurrentRadiusAttack; };
	UFUNCTION(BlueprintPure)
		float GetSpeedAttack() { return SpeedAttackCurrent; };
	UFUNCTION(BlueprintPure)
		int32 GetCurrentLevel() { return LevelBuild; };
	UFUNCTION(BlueprintCallAble)
		void SetCurrentLevelAndUPDStat(int32 NewLevel);
	UFUNCTION(BlueprintImplementableEvent)
		void InitBuild_BP();
	
};
