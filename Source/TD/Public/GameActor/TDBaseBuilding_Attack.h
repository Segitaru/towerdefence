// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameActor/TDBaseBuilding.h"
#include "TDBaseBuilding_Attack.generated.h"

/**
 * 
 */
UCLASS()
class TD_API ATDBaseBuilding_Attack : public ATDBaseBuilding
{
	GENERATED_BODY()
	
public:
	virtual void ImplemetationLogicTower() override;
};
