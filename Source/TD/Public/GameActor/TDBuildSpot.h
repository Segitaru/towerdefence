// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/FuncLibrary/Types.h"
#include "Particles/ParticleSystem.h"
#include "TDBuildSpot.generated.h"

UCLASS()
class TD_API ATDBuildSpot : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDBuildSpot();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USpotLightComponent* SpotLight;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereCollision = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MeshSpot = nullptr;
	UPROPERTY(BlueprintReadWrite)
		AActor* ActroOnSpot = nullptr;
	UPROPERTY()
		class ATDGameModeBase* GameModeRef = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UParticleSystem* EmitterDestroy = nullptr;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	void SpawnBuild(FBuildInfo BuildingInfo);
	UFUNCTION(BlueprintCallable)
	void DestroyBuild();
	void GameOver();
	UFUNCTION(BlueprintCallable)
	void ChangeVisibility(bool Visible);
};
