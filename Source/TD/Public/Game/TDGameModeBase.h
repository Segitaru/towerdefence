// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTimeTick, float, Time);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameOver);

UCLASS()
class TD_API ATDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
		FLevelInfo InfoLevels;

	UPROPERTY(BlueprintReadOnly)
		class ATDBaseSpawner* MiddleSpawner;
	UPROPERTY(BlueprintReadOnly)
		class ATDBaseSpawner* RightSpawner;
	UPROPERTY(BlueprintReadOnly)
		class ATDBaseSpawner* LeftSpawner;
	UPROPERTY(BlueprintReadOnly)
		TArray<ATDBaseSpawner*> SpawnerArray;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		class ATDPlayerController* PlayerRef = nullptr;
	UPROPERTY(BlueprintReadOnly)
		class ATDMainObject* MainObjectRef;
	UPROPERTY(BlueprintReadOnly)
		TArray<class ATDBuildSpot*> BuildSpotArray;
	UPROPERTY(BlueprintReadOnly)
		TArray<ATDBaseCharacter_Enemy*> EnemyArray;
	UPROPERTY(BlueprintReadOnly)
		class ATDPlayerState* PlayerStateRef = nullptr;

	UPROPERTY()
		class ATDGameStateBase* GameStateRef = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Sound Game")
		USoundBase* WinSound;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Sound Game")
		USoundBase* LoseSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Game")
		USoundBase* PreGameSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Game")
		USoundBase* InProgressGameSound;

	class UAudioComponent* PreGameComponent;
	class UAudioComponent* InProgressGameComponent;

	FTimerHandle TimerHandel_EnemySpawnTick;
	FTimerHandle TimerHandel_DeltaSpawnTick;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnEnemySetting")
		float EnemySpawnTick = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnEnemySetting")
		float EnemySpawnTickDelta = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnEnemySetting")
		float DeltaSpawnTick = 10.f;


	//TIME TO WAVE SWITCH
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnEnemySetting")
		float TimeWave1 = 30.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnEnemySetting")
		float TimeWave2 = 60.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnEnemySetting")
		float TimeWave3 = 90.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnEnemySetting")
		float TimeWave4 = 120.f;
protected:
	virtual void BeginPlay() override;
	//For protect system on overload
	float MinEnemySpawnTick = 0.25f;
	EWaveGame WaveGameValue = EWaveGame::Wave1;
	UDataTable* EnemyInfoTable = nullptr;
	int32 LocalValueSpawnedMob = 0;

public:


	void AddEnemy(class ATDBaseCharacter_Enemy* SpawnedEnemy);
	void AddSpawner(class ATDBaseSpawner* CreatedSpawner);
	void AddSpotBuild(class ATDBuildSpot* SpawnedSpot);
	void SetMainObject(class ATDMainObject* MainObjectCreated);

	bool GetEnemyInfoRow(int32 LevelEnemy, FEnemyInfo& OutInfo);
	UFUNCTION(BlueprintCallAble)
	void InitInfoEnemy(TArray<FName> EnemyName, UDataTable* EnemyInfo);	//Init from GameInstance BP
		TArray<FName> AllNameEnemy;
	UFUNCTION(BlueprintCallable)
		void SpawnEnemy(FEnemyInfo Info, ATDBaseSpawner* CreatedSpawner);
	UFUNCTION()
		void StageGameChange(EStageGame GameStage);

	UPROPERTY(BlueprintAssignable)
		FTimeTick OnTimeTick;
	UPROPERTY(BlueprintAssignable)
		FGameOver OnGameOver;

	UFUNCTION()
		void SwitchWave(float Time);

	UFUNCTION()
		void SpawnBoss(TSubclassOf<class ATDBaseCharacter> Boss);
	UFUNCTION()
		void StartWave(int32 ValueToSpawn);
	UFUNCTION()
		void StopWave();
		int32 ValueNeedSpawn;
		int32 ValueCurrentSpawn;
	UFUNCTION()
		void GameOver();
	UFUNCTION()
		void EnemyDestroed(AActor* EnemyRef);
	UFUNCTION()
		void SpawnEnemyStart();
	UFUNCTION()
		void SpawnEnemyTick();
	UFUNCTION()
		void DeltaSpawnTime();
	UFUNCTION()
		void DeltaSpawnTimeTick();

	UFUNCTION(BlueprintCallable)
		void BuildSelected();
	UFUNCTION(BlueprintCallable)
		void BuildSucces();

	UFUNCTION(BlueprintImplementableEvent)
		void CanUseNextWave_BP(bool State);
	UFUNCTION(BlueprintCallable)
		void StartWavePreTime();
};
