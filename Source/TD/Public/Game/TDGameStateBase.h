// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDGameStateBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStageSwitch, EStageGame, GameStage);

UCLASS()
class TD_API ATDGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;
	
public:
	UPROPERTY()
		FLevelInfo InfoLevels;
	UPROPERTY()
		class ATDGameModeBase* GameModeRef = nullptr;
	bool bWaveStart = false;
	UFUNCTION(BlueprintCallable)
		void SetInfoLevel(FLevelInfo Info) { InfoLevels = Info; };
	FLevelInfo GetInfoLevel() { return InfoLevels; };
	UPROPERTY(BlueprintReadOnly, Category = "LevelInfo")
		int32 FinalPhase = 3;
	UPROPERTY(BlueprintReadOnly, Category = "LevelInfo")
		TArray<int32> ScoreToWinPhase = { 5,10,15 };
	UPROPERTY(BlueprintReadOnly, Category = "LevelInfo")
		TSubclassOf<class ATDBaseCharacter> Boss;
	UPROPERTY(BlueprintReadOnly, Category = "LevelInfo")
		int32 AllStarsValue = 3;
	UPROPERTY(BlueprintReadOnly, Category = "LevelInfo")
		int32 CurrentStarsValue = 0;
	UPROPERTY(BlueprintReadOnly, Category = "LevelInfo")
		float AwardForStars = 1;
	
		int32 CurrentScore = 0;
	UPROPERTY(BlueprintReadOnly)
		int32 CurrentPhase = 0;









	UFUNCTION()
		void PreGameTime();
	UFUNCTION()
		void GameTime();
	UPROPERTY(ReplicatedUsing = StageGameChange_OnRep)
		EStageGame CurrentStageGame = EStageGame::PreGame;

	UFUNCTION(BlueprintCallable)
		void InitInfoLevels(FLevelInfo CurrentInfoLevels);


		void EnemyDestroy();
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float TimeGame = 5.f;
	FTimerHandle TimerHandle_ToStartGame;
	FTimerHandle TimerHandle_ToStartWave;
	FTimerHandle TimerHandle_GameInProgress;

	bool bPhaseChange = false;

	void PreStartWave();
		void WaveStart();
	UFUNCTION()
		void GameOver();
	UFUNCTION()
		void GameStarted();
	UPROPERTY(BlueprintAssignable)
		FStageSwitch OnStageGameSwitch;
	UFUNCTION()
		void StageGameChange_OnRep();
		virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
};
