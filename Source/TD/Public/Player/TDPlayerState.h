// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDPlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGoldChange, float, CurrentGold);

UCLASS()
class TD_API ATDPlayerState : public APlayerState
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
private:
	UPROPERTY(ReplicatedUsing = GoldChange_OnRep)
		float Gold = 0.f;



public:
	UPROPERTY(BlueprintAssignable)
		FGoldChange OnGoldChange;

	UPROPERTY()
		class ATDGameStateBase* GameStateRef = nullptr;

	FTimerHandle TimerHandle_TickGold;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		float GoldTickValue = 50.f;
	UFUNCTION()
		void GoldTickByValue();
	UFUNCTION(BlueprintCallable)
		void ChangeGoldByValue(float Value);
	UFUNCTION()
		void GoldChange_OnRep();
	UFUNCTION()
		void StageGameChange(EStageGame GameStage);
	UFUNCTION(BlueprintPure)
		float GetGoldPlayer() { return Gold; };
		virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
};
