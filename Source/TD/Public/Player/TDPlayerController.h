// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TD_API ATDPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
		EStageGame CurrentGameStage;
	UFUNCTION()
		void GameStateChange(EStageGame GameStage);
	UFUNCTION(BlueprintImplementableEvent)
		void GetGameState_BP();
	UFUNCTION(BlueprintImplementableEvent)
		void GameOverForController_BP(float TimeGame, bool IsWin, FLevelInfo InfoLevels, int32 ValueStar, float HealthBase);
};
