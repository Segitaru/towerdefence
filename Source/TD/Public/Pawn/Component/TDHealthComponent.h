// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeath, AActor*, Owner);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FHealthChange, float, CurrentHealt, float, MaxHealth, float, Damage);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TD_API UTDHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY()
		float MaxHealth = 100.f;
	UPROPERTY()
		float CurrentHealth = MaxHealth;
	UPROPERTY()
		bool IsAlive = true;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintPure)
	float GetMaxHealth() { return MaxHealth; };
	UFUNCTION(BlueprintPure)
	float GetCurrentHealth() {return CurrentHealth;};

	UFUNCTION(BlueprintPure)
		bool CheckAlive() { return IsAlive; };
	// �������: �����, ����� �������� ������ ��������� �������� � ������, ������������� ������� ����� �������
	UFUNCTION(BlueprintCallable)
		void ChangeHealth(float Value);
	UFUNCTION(BlueprintCallable)
		void SetMaxHealth(float Value);

	UPROPERTY(BlueprintAssignable, Category = "Death")
		FDeath OnDeath;
	UPROPERTY(BlueprintAssignable, Category = "Health")
		FHealthChange OnHealthChange;
		
};
