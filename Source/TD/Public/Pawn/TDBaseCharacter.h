// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDBaseCharacter.generated.h"

UCLASS()
class TD_API ATDBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDBaseCharacter();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UTDHealthComponent* HealthComponent;


	UPROPERTY(BlueprintReadWrite)
		bool bCanMove = true;
	UPROPERTY(BlueprintReadWrite)
		bool bCanAttack = true;
	UPROPERTY(BlueprintReadWrite)
		AActor* CurrentTarget = nullptr;

	UPROPERTY(BlueprintReadOnly)
		float BaseDamage = 10.f;
	UPROPERTY(BlueprintReadOnly)
		float BaseSpeedAttack = 1.f;
	UPROPERTY(BlueprintReadOnly)
		float BaseSpeedMoved = 300.f;
	UPROPERTY(BlueprintReadOnly)
		bool bIsSlowed = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual float TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* InstigatedBy, AActor* DamageCauser) override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void SetMoveState(bool IsCanMove);
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION()
	virtual void CharacterDeath(AActor* ActorRef);
	UFUNCTION(BlueprintCallable)
	virtual void MoveLogic(FVector& TargetTo);
	UFUNCTION(BlueprintImplementableEvent)
		void CharacterDeath_BP();
};
