// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/TDBaseCharacter.h"
#include "../Public/FuncLibrary/Types.h"
#include "../Public/FuncLibrary/TDInterfaceGameActor.h"
#include "TDBaseCharacter_Enemy.generated.h"

/**
 * 
 */
UCLASS()
class TD_API ATDBaseCharacter_Enemy : public ATDBaseCharacter, public ITDInterfaceGameActor
{
	GENERATED_BODY()

public:
	ATDBaseCharacter_Enemy();


	FEnemyInfo EnemyInfo;

	UPROPERTY()
		float CharacterHealth = 100.f;
	UPROPERTY()
		float CostingEnemy = 100.f;
	UPROPERTY()
		float DamageForBuilding = 100.f;

	UPROPERTY(BlueprintReadOnly)
		float SpeedDefault = BaseSpeedMoved;
	UPROPERTY()
		float SpeedMin = BaseSpeedMoved *0.2;
		bool bEnemyAttackBuild = false;

		class UParticleSystem* OverlapEmmiterWithBuild = nullptr;
		USoundBase* OverlapSound = nullptr;
		int32 PointIndex = 1;

	UPROPERTY()
		class ATDBaseSpawner* SpawnerRef;

	FTimerHandle TimeSlowdown;


		virtual void BeginPlay() override;
		void InitEnemy(FEnemyInfo &EnemyInfoInit, class ATDBaseSpawner* Spawner);

		virtual void Tick(float DeltaTime) override;
		virtual void MoveLogic(FVector &TargetTo) override;
	
		

	
		UFUNCTION()
			void OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
		UFUNCTION()
			void OnEnemyEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
		virtual void CharacterDeath(AActor* ActorRef) override;
		virtual void SlowdownEffect(float SlowdownValue, float SlowdownTime);
		void NormilizeSpeed();

};
