// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/TDBaseCharacter.h"
#include "../Public/FuncLibrary/Types.h"
#include "TDBaseCharacter_Defender.generated.h"

/**
 * 
 */
UCLASS()
class TD_API ATDBaseCharacter_Defender : public ATDBaseCharacter
{
	GENERATED_BODY()
public:
	ATDBaseCharacter_Defender();



	void InitDefender(FBuildInfo& DefenderInfoInit);
};
