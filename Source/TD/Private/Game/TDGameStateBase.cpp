// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/TDGameStateBase.h"
#include "Net/UnrealNetwork.h"
#include "../Public/Game/TDGameModeBase.h"
#include "../Public/Pawn/TDBaseCharacter.h"
#include "Kismet/KismetArrayLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "../public/Player/TDPlayerState.h"

void ATDGameStateBase::BeginPlay()
{
	Super::BeginPlay();
	GameModeRef = Cast<ATDGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ToStartGame, this, &ATDGameStateBase::PreGameTime, 1.f, true);
	GameModeRef->OnGameOver.AddDynamic(this, &ATDGameStateBase::GameOver);
}


void ATDGameStateBase::InitInfoLevels(FLevelInfo CurrentInfoLevels)
{
	InfoLevels = CurrentInfoLevels;
	FinalPhase = InfoLevels.FinalPhase;
	ScoreToWinPhase = InfoLevels.ScoreToWinPhase;
	Boss = InfoLevels.Boss;
	AllStarsValue = InfoLevels.AllStarsValue;
	CurrentStarsValue = InfoLevels.CurrentStarsValue;
	AwardForStars = InfoLevels.AwardForStars;
	
	
}

void ATDGameStateBase::PreGameTime()
{
	TimeGame -= 1.f;
	if (TimeGame <= 0.f)
	{
		GameStarted();
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ToStartGame);
	}
	if(GameModeRef)
		GameModeRef->OnTimeTick.Broadcast(TimeGame);

}

void ATDGameStateBase::GameOver()
{
	CurrentStageGame = EStageGame::PostGame;
	ATDGameStateBase::StageGameChange_OnRep();
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_GameInProgress);
}

void ATDGameStateBase::GameStarted()
{
	CurrentStageGame = EStageGame::Game;
	ATDGameStateBase::StageGameChange_OnRep();
	GameModeRef->CanUseNextWave_BP(true);
	//GameModeRef->StartWave(ScoreToWinPhase[0]);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_GameInProgress, this, &ATDGameStateBase::GameTime, 1.f, true);
}
void ATDGameStateBase::GameTime()
{
	TimeGame += 1.f;
	GameModeRef->OnTimeTick.Broadcast(TimeGame);
}

void ATDGameStateBase::EnemyDestroy()
{
	CurrentScore++;
	UE_LOG(LogTemp, Warning, TEXT("Current Score:  %i"), CurrentScore);
	if (ScoreToWinPhase.IsValidIndex(CurrentPhase))
	{
		if (CurrentScore >= ScoreToWinPhase[CurrentPhase] * 0.8)
			if(ScoreToWinPhase.IsValidIndex(CurrentPhase+1))
			GameModeRef->CanUseNextWave_BP(true);
		if (CurrentScore >= ScoreToWinPhase[CurrentPhase])
		{
			bPhaseChange = true;
			CurrentPhase++;
			GameModeRef->StopWave();
			
			if (ScoreToWinPhase.IsValidIndex(CurrentPhase))
			{
				UE_LOG(LogTemp, Warning, TEXT("NextWaveCommingSoon"));
				GetWorld()->GetTimerManager().SetTimer(TimerHandle_ToStartWave, this, &ATDGameStateBase::WaveStart, 5.f, false);
			}
			else
			{
				if (Boss)
					GameModeRef->SpawnBoss(Boss);
				else
					GameModeRef->OnGameOver.Broadcast();
			}
		}
	}
	
}



void ATDGameStateBase::StageGameChange_OnRep()
{
	OnStageGameSwitch.Broadcast(CurrentStageGame);
}
void ATDGameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATDGameStateBase, CurrentStageGame);
}

void ATDGameStateBase::WaveStart()
{
	
	bPhaseChange = false;
	GameModeRef->StartWave(ScoreToWinPhase[CurrentPhase]);
	GameModeRef->CanUseNextWave_BP(false);
}

void ATDGameStateBase::PreStartWave()
{
	bWaveStart = true;
	if (CurrentPhase == 0)
	{
		GameModeRef->StartWave(ScoreToWinPhase[CurrentPhase]);
		GameModeRef->CanUseNextWave_BP(false);
		return;
	}
	int32 LocalPhase = CurrentPhase;

	LocalPhase++;

	if (ScoreToWinPhase.IsValidIndex(LocalPhase))
	{
		if (TimerHandle_ToStartWave.IsValid())
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ToStartWave);
		if(!bPhaseChange)
			CurrentPhase=CurrentPhase+1;
		bPhaseChange = false;
		GameModeRef->StartWave(ScoreToWinPhase[CurrentPhase]);
	}
		GameModeRef->CanUseNextWave_BP(false);
	
}
