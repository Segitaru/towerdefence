// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/TDGameModeBase.h"
#include "../Public/Pawn/TDBaseCharacter_Enemy.h"
#include "../Public/GameActor/TDBaseSpawner.h"
#include "../Public/GameActor/TDBuildSpot.h"
#include "../Public/GameActor/TDMainObject.h"
#include "Kismet/GameplayStatics.h"
#include "../public/Player/TDPlayerController.h"
#include "../public/Game/TDGameStateBase.h"
#include "../public/Player/TDPlayerState.h"
#include "../Public/Pawn/Component/TDHealthComponent.h"
#include "Components/SpotLightComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Public/Pawn/TDBaseCharacter.h"
#include "Components/AudioComponent.h"

void ATDGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	

	PreGameComponent = UGameplayStatics::CreateSound2D(GetWorld(), PreGameSound);
	PreGameComponent->Play();
	PlayerRef = Cast<ATDPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(),0));
	PlayerStateRef = Cast<ATDPlayerState>(PlayerRef->PlayerState);
	GameStateRef = Cast<ATDGameStateBase>(UGameplayStatics::GetGameState(GetWorld()));
	if (GameStateRef)
	{
		GameStateRef->OnStageGameSwitch.AddDynamic(this, &ATDGameModeBase::StageGameChange);
	}
	OnGameOver.AddDynamic(this, &ATDGameModeBase::GameOver);
	OnTimeTick.AddDynamic(this, &ATDGameModeBase::SwitchWave);

}

void ATDGameModeBase::SwitchWave(float Time)
{
	if (Time <= TimeWave1)
		WaveGameValue = EWaveGame::Wave1;
	else if(Time<=TimeWave2)
		WaveGameValue = EWaveGame::Wave2;
	else if(Time<=TimeWave3)
		WaveGameValue = EWaveGame::Wave3;
	else if(Time<=TimeWave4)
		WaveGameValue = EWaveGame::Wave4;
}

void ATDGameModeBase::SpawnBoss(TSubclassOf<class ATDBaseCharacter> Boss)
{
}



void ATDGameModeBase::GameOver()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandel_EnemySpawnTick);
	GetWorld()->GetTimerManager().ClearTimer(TimerHandel_DeltaSpawnTick);
	for (int32 i = 0; i < EnemyArray.Num(); i++)
	{
		EnemyArray[i]->SetMoveState(false);
	}	
	for (int32 i = 0; i < BuildSpotArray.Num(); i++)
	{
		BuildSpotArray[i]->GameOver();
	}
	float MaxHealth = MainObjectRef->HealthComponent->GetMaxHealth();
	float CurrentHealth =MainObjectRef->HealthComponent->GetCurrentHealth();
	bool bIsWin = false;
	int32 ValueStar = 0;
	if (CurrentHealth >= 19.f)
	{
		bIsWin = true;
		ValueStar = 3;
	}
	else if (CurrentHealth >= 9.f)
	{
		bIsWin = true;
		ValueStar = 2;
	}
	else if (CurrentHealth > 0.f)
	{
		bIsWin = true;
		ValueStar = 1;
	}
	
	PlayerRef->GameOverForController_BP(GameStateRef->TimeGame, bIsWin, GameStateRef->GetInfoLevel(), ValueStar, CurrentHealth);
	if (bIsWin)
		UGameplayStatics::PlaySound2D(GetWorld(), WinSound);
	else
		UGameplayStatics::PlaySound2D(GetWorld(), LoseSound);
	//TODO: award
	//GameStateRef->AllStarsValue;
	//GameStateRef->CurrentStarsValue;
	//GameStateRef->AwardForStars;
}


void ATDGameModeBase::AddEnemy(ATDBaseCharacter_Enemy* SpawnedEnemy)
{
	SpawnedEnemy->HealthComponent->OnDeath.AddDynamic(this, &ATDGameModeBase::EnemyDestroed);
	EnemyArray.Add(SpawnedEnemy);
}

void ATDGameModeBase::EnemyDestroed(AActor* EnemyRef)
{
	ATDBaseCharacter_Enemy* Enemy = Cast<ATDBaseCharacter_Enemy>(EnemyRef);
	if (Enemy && !Enemy->bEnemyAttackBuild)
	{
		PlayerStateRef->ChangeGoldByValue(Enemy->CostingEnemy);
		GameStateRef->EnemyDestroy();
		UE_LOG(LogTemp, Warning, TEXT("Destroy"));

	}
	if (Enemy && Enemy->bEnemyAttackBuild)
	{
	//	if(GameStateRef->CurrentScore>0)
		//	GameStateRef->CurrentScore--;
		if(ValueCurrentSpawn > LocalValueSpawnedMob)
		ValueCurrentSpawn--;

		UE_LOG(LogTemp, Warning, TEXT("Local Value:  %i"), LocalValueSpawnedMob);
		UE_LOG(LogTemp, Warning, TEXT("Value Current Spawn:  %i"), ValueCurrentSpawn);
	}
	bool bIsFind = false;
	int32 i = 0;
	while (i < EnemyArray.Num() && !bIsFind)
	{
		if (EnemyArray[i] == EnemyRef)
		{
			EnemyArray.RemoveAt(i);
			bIsFind = true;
		}
		i++;
	}
}

// �������: �������� ���������� � ������
bool ATDGameModeBase::GetEnemyInfoRow(int32 LevelEnemy, FEnemyInfo& OutInfo)
{
	bool bIsFind = false;
	if (EnemyInfoTable)
	{
		FEnemyInfo* EnemyInfoRow;
		int8 i = 0;
		while (i < AllNameEnemy.Num() && !bIsFind)
		{
			EnemyInfoRow = EnemyInfoTable->FindRow<FEnemyInfo>(AllNameEnemy[i], "");
			if (EnemyInfoRow->LevelEnemy == LevelEnemy)
			{
				OutInfo = (*EnemyInfoRow);
				bIsFind = true;
			}
			i++;
		}

	}
	return bIsFind;
}


void ATDGameModeBase::SpawnEnemyStart()
{
	GetWorld()->GetTimerManager().SetTimer(TimerHandel_EnemySpawnTick, this, &ATDGameModeBase::SpawnEnemyTick, EnemySpawnTick, true, 0.f);
}


void ATDGameModeBase::SpawnEnemyTick()
{

	if (ValueCurrentSpawn >= ValueNeedSpawn)
		return;
		
	FEnemyInfo EnemyInfoRow;
	int32 RandomValueLevel;
	int32 RandomValueIndexSpawner;
	switch (WaveGameValue)
	{
	case EWaveGame::Wave1:
		RandomValueLevel = UKismetMathLibrary::RandomIntegerInRange(InfoLevels.MinLevelEnemy, InfoLevels.MaxLevelEnemy);
		if(GetEnemyInfoRow(RandomValueLevel, EnemyInfoRow))
			MiddleSpawner->SpawnEnemy(EnemyInfoRow);
			ValueCurrentSpawn++;



		break;
	case EWaveGame::Wave2:
		RandomValueLevel = UKismetMathLibrary::RandomIntegerInRange(InfoLevels.MinLevelEnemy, InfoLevels.MaxLevelEnemy);
		RandomValueIndexSpawner = UKismetMathLibrary::RandomIntegerInRange(0, SpawnerArray.Num()-1);

		if (GetEnemyInfoRow(RandomValueLevel, EnemyInfoRow))
			SpawnerArray[RandomValueIndexSpawner]->SpawnEnemy(EnemyInfoRow);
			ValueCurrentSpawn++;



		break;
	case EWaveGame::Wave3:
		RandomValueLevel = UKismetMathLibrary::RandomIntegerInRange(InfoLevels.MinLevelEnemy, InfoLevels.MaxLevelEnemy);
		RandomValueIndexSpawner = UKismetMathLibrary::RandomIntegerInRange(0, SpawnerArray.Num() - 1);

		if (GetEnemyInfoRow(RandomValueLevel, EnemyInfoRow))
			SpawnerArray[RandomValueIndexSpawner]->SpawnEnemy(EnemyInfoRow);
			ValueCurrentSpawn++;
		if (SpawnerArray.IsValidIndex(RandomValueIndexSpawner + 1))
		{
			if (GetEnemyInfoRow(0, EnemyInfoRow))
			{
				SpawnerArray[RandomValueIndexSpawner + 1]->SpawnEnemy(EnemyInfoRow);
				ValueCurrentSpawn++;
				if (ValueCurrentSpawn >= ValueNeedSpawn)
					return;
			}
		}
		else if(SpawnerArray.IsValidIndex(RandomValueIndexSpawner -1))
			if (GetEnemyInfoRow(RandomValueLevel, EnemyInfoRow))
			{
				SpawnerArray[RandomValueIndexSpawner - 1]->SpawnEnemy(EnemyInfoRow);
				ValueCurrentSpawn++;
			}
		break;
	case EWaveGame::Wave4:
		RandomValueLevel = UKismetMathLibrary::RandomIntegerInRange(InfoLevels.MinLevelEnemy, InfoLevels.MaxLevelEnemy);

		if (GetEnemyInfoRow(RandomValueLevel, EnemyInfoRow))
			for (int32 i = 0; i < SpawnerArray.Num(); i++)
			{
				SpawnerArray[i]->SpawnEnemy(EnemyInfoRow);
				ValueCurrentSpawn++;
				if (ValueCurrentSpawn >= ValueNeedSpawn)
					return;
			}
		break;
	default:
		UE_LOG(LogTemp, Warning, TEXT("ATDGameModeBase::SpawnEnemyTick - Some wrong, enemy not spawning"))
		break;
	}

	
}


void ATDGameModeBase::DeltaSpawnTime()
{
	//GetWorld()->GetTimerManager().SetTimer(TimerHandel_DeltaSpawnTick, this, &ATDGameModeBase::DeltaSpawnTimeTick, DeltaSpawnTick, true);
}
void ATDGameModeBase::DeltaSpawnTimeTick()
{
	EnemySpawnTick -= EnemySpawnTickDelta;
	GetWorld()->GetTimerManager().ClearTimer(TimerHandel_EnemySpawnTick);
	if (EnemySpawnTick <= MinEnemySpawnTick)
	{
		EnemySpawnTick = MinEnemySpawnTick;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandel_DeltaSpawnTick);
	}
	SpawnEnemyStart();
}
void ATDGameModeBase::BuildSelected()
{
	for (int i = 0; i < BuildSpotArray.Num(); i++)
	{
		if (!BuildSpotArray[i]->ActroOnSpot)
			BuildSpotArray[i]->SpotLight->SetVisibility(true);
	}
}
void ATDGameModeBase::AddSpawner(ATDBaseSpawner* CreatedSpawner)
{
	switch (CreatedSpawner->LineSpawner)
	{
	case ELineSpawner::Left:
		LeftSpawner = CreatedSpawner;		
		break;
	case ELineSpawner::Middle:
		MiddleSpawner = CreatedSpawner;
		break;
	case ELineSpawner::Right:
		RightSpawner = CreatedSpawner;
		break;
	default:
		break;
	}
	SpawnerArray.Add(CreatedSpawner);
}

void ATDGameModeBase::AddSpotBuild(ATDBuildSpot* SpawnedSpot)
{
	BuildSpotArray.Add(SpawnedSpot);
}

void ATDGameModeBase::SetMainObject(ATDMainObject* MainObjectCreated)
{
	MainObjectRef = MainObjectCreated;
	if (!MainObjectRef)
		UE_LOG(LogTemp, Warning, TEXT("ATDGameModeBase::SetMainObject - FAIL SET MAIN OBJECT"));
}

void ATDGameModeBase::BuildSucces()
{
	for (int i = 0; i < BuildSpotArray.Num(); i++)
	{
		if (!BuildSpotArray[i]->ActroOnSpot)
			BuildSpotArray[i]->SpotLight->SetVisibility(false);
	}
}


void ATDGameModeBase::StartWave(int32 ValueToSpawn)
{
	ValueNeedSpawn = ValueToSpawn;
	SpawnEnemyStart();
	if (GameStateRef->CurrentPhase > 0)
		LocalValueSpawnedMob = GameStateRef->ScoreToWinPhase[GameStateRef->CurrentPhase - 1];
	else
		LocalValueSpawnedMob = 0;
}

void ATDGameModeBase::StopWave()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandel_EnemySpawnTick);
}


void ATDGameModeBase::StartWavePreTime()
{
	GameStateRef->PreStartWave();
}

void ATDGameModeBase::InitInfoEnemy(TArray<FName> EnemyName, UDataTable* EnemyInfo)
{

	AllNameEnemy = EnemyName;
	EnemyInfoTable = EnemyInfo;
}

void ATDGameModeBase::SpawnEnemy(FEnemyInfo Info, ATDBaseSpawner* CreatedSpawner)
{
	CreatedSpawner->SpawnEnemy(Info);
}
void ATDGameModeBase::StageGameChange(EStageGame GameStage)
{
	switch (GameStage)
	{
	case EStageGame::PreGame:
		
		break;
	case EStageGame::Game:
		if (PreGameComponent)
			PreGameComponent->Stop();
	
		InfoLevels = GameStateRef->InfoLevels;
		

		UGameplayStatics::PlaySound2D(GetWorld(), InProgressGameSound);
		PlayerRef->GameStateChange(GameStage);
		DeltaSpawnTime();
		break;
	case EStageGame::PostGame:
		PlayerRef->GameStateChange(GameStage);
		for (int i = 0; i < EnemyArray.Num(); i++)
		{
			if (EnemyArray[i])
			{

			}
		}
		break;
	default:
		break;
	}
}