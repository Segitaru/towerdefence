// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/TDPlayerState.h"
#include "../public/Game/TDGameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

void ATDPlayerState::BeginPlay()
{
	Super::BeginPlay();
	OnGoldChange.Broadcast(Gold);
	GameStateRef = Cast<ATDGameStateBase>(UGameplayStatics::GetGameState(GetWorld()));
	if (GameStateRef)
	{
		GameStateRef->OnStageGameSwitch.AddDynamic(this, &ATDPlayerState::StageGameChange);
	}
	ChangeGoldByValue(GameStateRef->InfoLevels.StartedValueExp);
}
void ATDPlayerState::GoldTickByValue()
{
	if (GameStateRef->bWaveStart)
	{
		Gold = Gold + GoldTickValue;
		ATDPlayerState::GoldChange_OnRep();
	}
}
void ATDPlayerState::ChangeGoldByValue(float Value)
{
	Gold += Value;
	ATDPlayerState::GoldChange_OnRep();
}

void ATDPlayerState::GoldChange_OnRep()
{
	OnGoldChange.Broadcast(Gold);
}

void ATDPlayerState::StageGameChange(EStageGame GameStage)
{
	switch (GameStage)
	{
	case EStageGame::PreGame:
		OnGoldChange.Broadcast(Gold);
		break;
	case EStageGame::Game:
		//GetWorld()->GetTimerManager().SetTimer(TimerHandle_TickGold, this, &ATDPlayerState::GoldTickByValue, 1.f, true);
		break;
	case EStageGame::PostGame:
		//GetWorld()->GetTimerManager().ClearTimer(TimerHandle_TickGold);
		break;
	default:
		break;
	}
}
void ATDPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATDPlayerState, Gold);
}