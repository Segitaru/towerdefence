// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile/TDProjectileBase_AOE.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

void ATDProjectileBase_AOE::AdditionalLogicProjectile(const FHitResult& Hit)
{

	TArray<AActor*> ActorToIgnore;
	ActorToIgnore.Add(this);
	TArray<FHitResult> HitInfo;
	FVector LocationHit = Hit.GetActor()->GetActorLocation();
	UKismetSystemLibrary::SphereTraceMultiByProfile(GetWorld(), LocationHit, LocationHit, ProjectileSetting.Projectile_AOE_RadiusDamage,
														"Pawn", false, ActorToIgnore, EDrawDebugTrace::None, HitInfo,true);
	for (int i = 0; i < HitInfo.Num(); i++)
	{
		if (HitInfo[i].GetActor()->ActorHasTag("Enemy"))
		{
			UGameplayStatics::ApplyDamage(HitInfo[i].GetActor(), ProjectileSetting.ProjectileDamage, nullptr, nullptr, NULL);
			AdditionalLogicProjectile_AOE(HitInfo[i]);
		}
	}

}

