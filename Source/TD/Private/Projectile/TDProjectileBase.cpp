// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile/TDProjectileBase.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "../Public/Pawn/TDBaseCharacter_Enemy.h"
#include "../Public/Pawn/Component/TDHealthComponent.h"
// Sets default values
ATDProjectileBase::ATDProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	RootComponent = SphereCollision;
	SphereCollision->SetSphereRadius(16.f);
	SphereCollision->SetCollisionProfileName("BlockAll");

	SphereCollision->bReturnMaterialOnMove = true;
	SphereCollision->SetCanEverAffectNavigation(false);
	SphereCollision->SetSimulatePhysics(true);
	SphereCollision->SetEnableGravity(false);
	SphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	StaticMeshComponent->SetupAttachment(SphereCollision);
	StaticMeshComponent->SetCanEverAffectNavigation(false);
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	ProjectileFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile FX"));
	ProjectileFX->SetupAttachment(SphereCollision);
}

// Called when the game starts or when spawned
void ATDProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	SphereCollision->OnComponentHit.AddDynamic(this, &ATDProjectileBase::ProjectileCollisionSphereHit);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ATDProjectileBase::OnProjectileBeginOverlap);
	
		

}

// Called every frame
void ATDProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (TargetToKill && this)
	{
		FRotator ActorRotation = GetActorRotation();
		FVector ActorLocation = GetActorLocation();
		FVector TargetLocation = TargetToKill->GetActorLocation();
		AddActorLocalOffset(FVector(ProjectileSetting.ProjectileSpeed,0,0));
		SetActorRotation(UKismetMathLibrary::RInterpTo(ActorRotation, UKismetMathLibrary::FindLookAtRotation(ActorLocation,TargetLocation),
							UGameplayStatics::GetWorldDeltaSeconds(GetWorld()), 20.f)); // 20.f can change for slow or fast interpolation
	}
	else
		this->Destroy();
}
void ATDProjectileBase::ProjectileInit(AActor* TargetToKillRef, FProjectileInfo ProjectileSettingToInit)
{
	TargetToKill = TargetToKillRef;
	ProjectileSetting = ProjectileSettingToInit;
	this->SetLifeSpan(ProjectileSetting.ProjectileLifeTime);
	if (TargetToKill)
	{
		UTDHealthComponent* HealthCompoent = dynamic_cast<UTDHealthComponent*>(TargetToKill->GetComponentByClass(UTDHealthComponent::StaticClass()));
		if (HealthCompoent)
			HealthCompoent->OnDeath.AddDynamic(this, &ATDProjectileBase::DestroyProjectile);
	}
}
void ATDProjectileBase::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	
}

void ATDProjectileBase::AdditionalLogicProjectile(const FHitResult& Hit)
{
	UGameplayStatics::ApplyDamage(Hit.GetActor(), ProjectileSetting.ProjectileDamage, nullptr, nullptr, NULL);
}

bool ATDProjectileBase::CheckTagsActor(AActor* OtherActor)
{
	if (OtherActor->ActorHasTag("Enemy"))
		return true;
	else
		return false;
}

void ATDProjectileBase::DestroyProjectile(AActor* TargetOwner)
{
	Destroy();
}

void ATDProjectileBase::OnProjectileBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (CheckTagsActor(OtherActor))
	{
		if (ProjectileSetting.ProjectileHitSound)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ProjectileHitSound, SweepResult.ImpactPoint);
		if (ProjectileSetting.ProjectileHitFX)
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ProjectileHitFX, FTransform(FRotator(0),OtherActor->GetActorLocation(), ProjectileSetting.ScaleProjectile));
		AdditionalLogicProjectile(SweepResult);
	}
	this->Destroy();
}
