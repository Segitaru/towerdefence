// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile/TDProjectileBase_AOE_Slowdown.h"
#include "../Public/FuncLibrary/TDInterfaceGameActor.h"
void ATDProjectileBase_AOE_Slowdown::AdditionalLogicProjectile_AOE(const FHitResult& Hit)
{
	ITDInterfaceGameActor* Interface = Cast<ITDInterfaceGameActor>(Hit.GetActor());
	if (Interface)
	{
		Interface->SlowdownEffect(ProjectileSetting.Projectile_AOE_SlowdownValue, ProjectileSetting.Projectile_AOE_SlowdownTime);
	}
}