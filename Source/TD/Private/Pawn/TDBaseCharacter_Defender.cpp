// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/TDBaseCharacter_Defender.h"
#include "../Public/Pawn/Component/TDHealthComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

ATDBaseCharacter_Defender::ATDBaseCharacter_Defender()
{
	this->Tags.Add("Defender");
}

void ATDBaseCharacter_Defender::InitDefender(FBuildInfo& DefenderInfoInit)
{
	HealthComponent->SetMaxHealth(DefenderInfoInit.HealthDefender);
	BaseDamage = DefenderInfoInit.Damage;
	BaseSpeedAttack = DefenderInfoInit.SpeedAttackDefender;
	BaseSpeedMoved = DefenderInfoInit.SpeedDefaultMovement;
}
