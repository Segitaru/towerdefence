// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Component/TDHealthComponent.h"

// Sets default values for this component's properties
UTDHealthComponent::UTDHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTDHealthComponent::ChangeHealth(float Value)
{
	if (IsAlive)
	{
		CurrentHealth += Value;
		if (CurrentHealth <= 0)
		{
			CurrentHealth = 0;
			IsAlive = false;
			OnHealthChange.Broadcast(CurrentHealth, MaxHealth, Value);
			OnDeath.Broadcast(GetOwner());
		}
		else
		{
			OnHealthChange.Broadcast(CurrentHealth, MaxHealth, Value);
		}
	}
}

void UTDHealthComponent::SetMaxHealth(float Value)
{
	MaxHealth = Value;
	CurrentHealth = MaxHealth;
}

