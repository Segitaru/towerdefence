// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/TDBaseCharacter_Enemy.h"
#include "../Public/Pawn/Component/TDHealthComponent.h"
#include "../public/GameActor/TDBaseSpawner.h"
#include "Components/SplineComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "Particles/ParticleSystem.h"

ATDBaseCharacter_Enemy::ATDBaseCharacter_Enemy()
{
	
	
	this->Tags.Add("Enemy");
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ATDBaseCharacter_Enemy::BeginPlay()
{
	Super::BeginPlay();
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ATDBaseCharacter_Enemy::OnEnemyBeginOverlap);

	
}
void ATDBaseCharacter_Enemy::OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag("PlayerBuild"))
	{
		bEnemyAttackBuild = true;
		UGameplayStatics::ApplyDamage(OtherActor, DamageForBuilding, nullptr, nullptr, NULL);
		HealthComponent->ChangeHealth(-HealthComponent->GetMaxHealth());
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Ignore);
		if(OverlapEmmiterWithBuild)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), OverlapEmmiterWithBuild, FTransform(FRotator(0), GetActorLocation(), FVector(4.f)));
		if(OverlapSound)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), OverlapSound, GetActorLocation());
	}
}

void ATDBaseCharacter_Enemy::CharacterDeath(AActor* ActorRef)
{
	Super::CharacterDeath(ActorRef);
}

void ATDBaseCharacter_Enemy::OnEnemyEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}
void ATDBaseCharacter_Enemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}



void ATDBaseCharacter_Enemy::MoveLogic(FVector& TargetTo)
{
	if (SpawnerRef)
	{
		FVector LocationPoint = SpawnerRef->SplineComponent->USplineComponent::GetLocationAtSplinePoint(PointIndex, ESplineCoordinateSpace::World);
		FVector LocationActor = GetActorLocation();
		if (UKismetMathLibrary::Vector_Distance(LocationActor, LocationPoint) <= 450.f) // 50 distance to switch on next point
		{

			if(PointIndex <SpawnerRef->SplineComponent->GetNumberOfSplinePoints())
			PointIndex++;
			TargetTo = SpawnerRef->SplineComponent->USplineComponent::GetLocationAtSplinePoint(PointIndex, ESplineCoordinateSpace::World);
		}
		else
		{
			TargetTo = LocationPoint;
			//AddActorLocalOffset(FVector(BaseSpeed, 0, 0));

			//FRotator RotationToTarget = UKismetMathLibrary::RInterpTo(RotationActor, UKismetMathLibrary::FindLookAtRotation(LocationActor,LocationPoint),
			//UGameplayStatics::GetWorldDeltaSeconds(GetWorld()), 10.f);

			//SetActorRotation(RotationToTarget); // 20.f can change for slow or fast interpolation
		}
	}
	
}

void ATDBaseCharacter_Enemy::SlowdownEffect(float SlowdownValue, float SlowdownTime)
{
	BaseSpeedMoved *= SlowdownValue;
	bIsSlowed = true;
	if (BaseSpeedMoved <= SpeedMin)
		BaseSpeedMoved = SpeedMin;
	GetWorldTimerManager().SetTimer(TimeSlowdown,this, &ATDBaseCharacter_Enemy::NormilizeSpeed, SlowdownTime, false);
	
		
}

void ATDBaseCharacter_Enemy::NormilizeSpeed()
{
	if (TimeSlowdown.IsValid())
		GetWorldTimerManager().ClearTimer(TimeSlowdown);
	BaseSpeedMoved = SpeedDefault;
	bIsSlowed = false;

}

void ATDBaseCharacter_Enemy::InitEnemy(FEnemyInfo& EnemyInfoInit, ATDBaseSpawner* Spawner)
{
	
	CharacterHealth = EnemyInfoInit.CharacterHealth;
	HealthComponent->SetMaxHealth(CharacterHealth);
	CostingEnemy = EnemyInfoInit.CostingEnemy;
	DamageForBuilding = EnemyInfoInit.DamageForBuilding;
	BaseDamage = EnemyInfoInit.DamageToActor;
	BaseSpeedMoved = EnemyInfoInit.BaseSpeed;
	SpeedDefault = BaseSpeedMoved = EnemyInfoInit.BaseSpeed;
	SpawnerRef = Spawner;
	OverlapEmmiterWithBuild = EnemyInfoInit.OverlapEmmiter;
	OverlapSound = EnemyInfoInit.OverlapSound;
}




