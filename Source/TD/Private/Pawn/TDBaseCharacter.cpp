// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/TDBaseCharacter.h"
#include "../Public/Pawn/Component/TDHealthComponent.h"
// Sets default values
ATDBaseCharacter::ATDBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	HealthComponent = CreateDefaultSubobject<UTDHealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void ATDBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	HealthComponent->OnDeath.AddDynamic(this, &ATDBaseCharacter::CharacterDeath);
}

// Called every frame
void ATDBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ATDBaseCharacter::TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* InstigatedBy, AActor* DamageCauser)
{
	HealthComponent->ChangeHealth(-Damage);
	return Damage;
}

void ATDBaseCharacter::CharacterDeath(AActor* ActorRef)
{
	SetMoveState(false);
	bCanAttack = false;
	CharacterDeath_BP();
}

void ATDBaseCharacter::MoveLogic(FVector& TargetTo)
{
}

void ATDBaseCharacter::SetMoveState(bool IsCanMove)
{
	bCanMove = IsCanMove;
}