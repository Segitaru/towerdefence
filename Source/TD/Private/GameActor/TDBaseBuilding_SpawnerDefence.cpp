// Fill out your copyright notice in the Description page of Project Settings.


#include "GameActor/TDBaseBuilding_SpawnerDefence.h"
#include "Pawn/TDBaseCharacter_Defender.h"
#include "Components/ArrowComponent.h"
#include "../Public/Pawn/Component/TDHealthComponent.h"

void ATDBaseBuilding_SpawnerDefence::ImplemetationLogicTower()
{
	if (ArrayDefender.Num() < InfoBuilding.MaxValueDefender)
	{
		if (TargetToKill)
		{
			FActorSpawnParameters SpawnParametrs;
			SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			ATDBaseCharacter_Defender* SpawnedDefender = GetWorld()->SpawnActor<ATDBaseCharacter_Defender>(InfoBuilding.DefenderPawn, ArrowComponent->GetComponentTransform(), SpawnParametrs);
			
			if (SpawnedDefender)
			{
				SpawnedDefender->InitDefender(InfoBuilding);
				ArrayDefender.Add(SpawnedDefender);
				SpawnedDefender->HealthComponent->OnDeath.AddDynamic(this, &ATDBaseBuilding_SpawnerDefence::DefenderDeath);
			}
				
		}
	}
	
}

//TODO: ���������� ������� ����������� ������ � ������ ���������
void ATDBaseBuilding_SpawnerDefence::DefenderDeath(AActor* TargetDefender)
{
	ATDBaseCharacter_Defender* Defender = Cast<ATDBaseCharacter_Defender>(TargetDefender);
	if(Defender)
	ArrayDefender.Remove(Defender);
}

void ATDBaseBuilding_SpawnerDefence::TowerLogicStop()
{
	for (int i = 0; i < ArrayDefender.Num(); i++)
	{
		ArrayDefender[i]->bCanMove = false;
		ArrayDefender[i]->bCanAttack = false;
	}
}
