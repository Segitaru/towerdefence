// Fill out your copyright notice in the Description page of Project Settings.


#include "GameActor/TDBuildSpot.h"
#include "Components/SpotLightComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "../Public/Game/TDGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/GameActor/TDBaseBuilding.h"
// Sets default values
ATDBuildSpot::ATDBuildSpot()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshSpot = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshSpot"));
	RootComponent = MeshSpot;
	MeshSpot->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SphereCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	SphereCollision->SetSphereRadius(100.f);
	SphereCollision->AttachTo(RootComponent);

	SpotLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("SpotLight"));
	SpotLight->SetVisibility(false);
	SpotLight->AttachTo(RootComponent);	
	this->Tags.Add("BuildSpot");
}

// Called when the game starts or when spawned
void ATDBuildSpot::BeginPlay()
{
	Super::BeginPlay();
	GameModeRef = Cast<ATDGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GameModeRef->AddSpotBuild(this);
}

// Called every frame
void ATDBuildSpot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDBuildSpot::SpawnBuild(FBuildInfo BuildingInfo)
{
	if (!ActroOnSpot)
	{
		FActorSpawnParameters SpawnParametrs;
		SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ATDBaseBuilding* SpawnedBuild = GetWorld()->SpawnActor<ATDBaseBuilding>(BuildingInfo.BuildingClass, GetActorTransform(), SpawnParametrs);
		if (SpawnedBuild)
		{
			SpawnedBuild->InitBuild(BuildingInfo);
			ActroOnSpot = SpawnedBuild;
			GameModeRef->BuildSucces();
			if (SpotLight)
				SpotLight->SetVisibility(false);
		}
	}
}

void ATDBuildSpot::DestroyBuild()
{
	if (ActroOnSpot)
		ActroOnSpot->Destroy();
}

void ATDBuildSpot::GameOver()
{
	if (ActroOnSpot)
		Cast<ATDBaseBuilding>(ActroOnSpot)->TowerLogicStop();
	if (EmitterDestroy)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EmitterDestroy, GetActorTransform());
}

void ATDBuildSpot::ChangeVisibility(bool Visible)
{
	if (!ActroOnSpot)
	{
		if (SpotLight)
			SpotLight->SetVisibility(Visible);
	}
}



