// Fill out your copyright notice in the Description page of Project Settings.


#include "GameActor/TDMainObject.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "../Public/Pawn/Component/TDHEalthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/Game/TDGameModeBase.h"
// Sets default values
ATDMainObject::ATDMainObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	RootComponent = SphereCollision;
	HealthComponent = CreateDefaultSubobject<UTDHealthComponent>(TEXT("HealthComponent"));
	MeshBuild = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshBuild"));
	MeshBuild->AttachTo(SphereCollision);
	this->Tags.Add("PlayerBuild");
}

// Called when the game starts or when spawned
void ATDMainObject::BeginPlay()
{
	Super::BeginPlay();
	HealthComponent->OnDeath.AddDynamic(this, &ATDMainObject::MainBuildDestroed);
	GameModeRef = Cast<ATDGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GameModeRef->SetMainObject(this);
}

// Called every frame
void ATDMainObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDMainObject::MainBuildDestroed(AActor* MainBuildRef)
{
	GameModeRef->OnGameOver.Broadcast();

}

float ATDMainObject::TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* InstigatedBy, AActor* DamageCauser)
{
	HealthComponent->ChangeHealth(-Damage);
	return Damage;
}