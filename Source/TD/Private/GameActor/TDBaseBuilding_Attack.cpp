// Fill out your copyright notice in the Description page of Project Settings.


#include "GameActor/TDBaseBuilding_Attack.h"
#include "../Public/Projectile/TDProjectileBase.h"
#include "Components/ArrowComponent.h"

void ATDBaseBuilding_Attack::ImplemetationLogicTower()
{
	if (TargetToKill)
	{
		FActorSpawnParameters SpawnParametrs;
		SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ATDProjectileBase* SpawnedProjectile = GetWorld()->SpawnActor<ATDProjectileBase>(InfoBuilding.PorjectileBuild, ArrowComponent->GetComponentTransform(), SpawnParametrs);
		if (SpawnedProjectile)
		{
			SpawnedProjectile->ProjectileInit(TargetToKill, InfoBuilding.ProjectileSetting);
		}
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_Shooting);
	}
}
