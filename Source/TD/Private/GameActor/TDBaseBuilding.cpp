// Fill out your copyright notice in the Description page of Project Settings.


#include "GameActor/TDBaseBuilding.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "../Public/Pawn/TDBaseCharacter.h"
#include "../Public/GameActor/WeaponActor.h"
#include "../Public/Projectile/TDProjectileBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Public/Pawn/TDBaseCharacter_Enemy.h"
#include "../Public/Pawn/Component/TDHealthComponent.h"
// Sets default values
ATDBaseBuilding::ATDBaseBuilding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	MeshBuild = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshBuild"));
	MeshBuild->AttachTo(RootComponent);
	MeshBuild->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision -> AttachTo(RootComponent);
	SphereCollision->SetSphereRadius(800);

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	ArrowComponent -> AttachTo(RootComponent);

	SceneComponentSpawned = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponentSpawned"));
	SceneComponentSpawned->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void ATDBaseBuilding::BeginPlay()
{
	Super::BeginPlay();
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ATDBaseBuilding::OnEnemyBeginOverlap);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &ATDBaseBuilding::OnEnemyEndOverlap);


}

// Called every frame
void ATDBaseBuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDBaseBuilding::InitBuild(FBuildInfo BuildingInfo)
{
	InfoBuilding = BuildingInfo;
	CurrentRadiusAttack = InfoBuilding.RadiusAttack1;
	SphereCollision->SetSphereRadius(CurrentRadiusAttack);
	SpeedAttackCurrent = InfoBuilding.SpeedAttack1;

	if (InfoBuilding.ActorOnBuild)
	{
		FActorSpawnParameters SpawnParametrs;
		SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		GetWorld()->SpawnActor<AWeaponActor>(InfoBuilding.ActorOnBuild, SceneComponentSpawned->GetComponentTransform(), SpawnParametrs);
	}
	if (InfoBuilding.CharacterOnBuild)
	{
		FActorSpawnParameters SpawnParametrs;
		SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		GetWorld()->SpawnActor<ATDBaseCharacter>(InfoBuilding.CharacterOnBuild, SceneComponentSpawned->GetComponentTransform(), SpawnParametrs);
	}
	InitBuild_BP();
}
//TODO: ������� ��������� ���� � ������� �����
void ATDBaseBuilding::OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag("Enemy"))
	{
		ArrayTarget.Add(OtherActor);
		if (!TargetToKill)
		{
			TargetToKill = OtherActor;
			TowerLogicStart();
		}
	}

}

void ATDBaseBuilding::OnEnemyEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

	if (OtherActor && OtherActor->ActorHasTag("Enemy") && OtherActor == TargetToKill)
	{
		if (ArrayTarget.Num() > 0)
		{
			ArrayTarget.Remove(OtherActor);
			int32 IndexEnemy = UKismetMathLibrary::RandomIntegerInRange(0, ArrayTarget.Num() - 1);
			if (ArrayTarget.IsValidIndex(IndexEnemy))
				TargetToKill = ArrayTarget[IndexEnemy];
			else
				TargetToKill = nullptr;
		}
	}
	else if(OtherActor && OtherActor->ActorHasTag("Enemy"))
		ArrayTarget.Remove(OtherActor);
}

void ATDBaseBuilding::TowerLogicStart()
{
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_Shooting, this, &ATDBaseBuilding::TowerLogicTick, InfoBuilding.SpeedAttack1, true);
}

void ATDBaseBuilding::TowerLogicTick()
{
	ImplemetationLogicTower();
}

void ATDBaseBuilding::ImplemetationLogicTower()
{
}

//void ATDBaseBuilding::TargetKill(AActor* EnemyOwner)
//{
//
//	if (EnemyOwner == TargetToKill)
//	{
//		ArrayTarget.Remove(EnemyOwner);
//		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_Shooting);
//		if (ArrayTarget.Num() > 0)
//		TargetToKill = ArrayTarget[UKismetMathLibrary::RandomIntegerInRange(0, ArrayTarget.Num() - 1)];
//		else
//			TargetToKill = nullptr;
//		TowerLogicStart();
//	}
//	else if (EnemyOwner->ActorHasTag("Enemy"))
//		ArrayTarget.Remove(EnemyOwner);
//}

void ATDBaseBuilding::TowerLogicStop()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_Shooting);
}

void ATDBaseBuilding::SetCurrentLevelAndUPDStat(int32 NewLevel)
{
	switch (NewLevel)
	{
	case 2:
		LevelBuild = NewLevel;
		SpeedAttackCurrent = InfoBuilding.SpeedAttack2;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_Shooting);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Shooting, this, &ATDBaseBuilding::TowerLogicTick, SpeedAttackCurrent, true);
	
		CurrentRadiusAttack = InfoBuilding.RadiusAttack2;
		SphereCollision->SetSphereRadius(CurrentRadiusAttack);
		InitBuild_BP();
		break;
	case 3:
		LevelBuild = NewLevel;
		SpeedAttackCurrent = InfoBuilding.SpeedAttack3;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_Shooting);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Shooting, this, &ATDBaseBuilding::TowerLogicTick, SpeedAttackCurrent, true);
		CurrentRadiusAttack = InfoBuilding.RadiusAttack3;
		SphereCollision->SetSphereRadius(CurrentRadiusAttack);
		InitBuild_BP();
		break;
	default:
		break;
	}
}
