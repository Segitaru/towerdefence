// Fill out your copyright notice in the Description page of Project Settings.


#include "GameActor/TDBaseSpawner.h"
#include "Components/SplineComponent.h"
#include "../Public/Pawn/TDBaseCharacter_Enemy.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/Game/TDGameModeBase.h"
#include "Kismet/KismetMathLibrary.h"
// Sets default values
ATDBaseSpawner::ATDBaseSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
}

// Called when the game starts or when spawned
void ATDBaseSpawner::BeginPlay()
{
	Super::BeginPlay();
	GameModeRef = Cast<ATDGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GameModeRef->AddSpawner(this);
}

// Called every frame
void ATDBaseSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDBaseSpawner::SpawnEnemy(FEnemyInfo Info)
{
	// ��� �������� ������ ������!
	int32 n = 10;
	FActorSpawnParameters SpawnParametrs;
	SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	FVector LocationToSpawn = FVector(GetActorLocation().X + UKismetMathLibrary::RandomFloatInRange(-10.f*n, +10.f*n), GetActorLocation().Y + UKismetMathLibrary::RandomFloatInRange(-10.f*n, +10.f*n), GetActorLocation().Z);

	FTransform Transform = FTransform(GetActorRotation(), LocationToSpawn, GetActorScale());
	ATDBaseCharacter_Enemy* SpawnedEnemy = GetWorld()->SpawnActor<ATDBaseCharacter_Enemy>(Info.EnemyClass, Transform, SpawnParametrs);
	if (SpawnedEnemy)
	{
		SpawnedEnemy->InitEnemy(Info, this);
		GameModeRef->AddEnemy(SpawnedEnemy);
	}
		
	
}